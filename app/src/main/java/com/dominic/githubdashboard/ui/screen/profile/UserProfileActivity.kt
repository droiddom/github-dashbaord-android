package com.dominic.githubdashboard.ui.screen.profile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.Transformations.map
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.Glide
import com.dominic.github_api_consumer.GithubApiConsumer
import com.dominic.githubdashboard.R
import com.dominic.githubdashboard.mvp.model.GithubProfileModelInterfaceImpl
import com.dominic.githubdashboard.mvp.presenter.GithubProfilePresenter
import com.dominic.githubdashboard.mvp.presenter.GithubProfilePresenterImpl
import com.dominic.githubdashboard.mvp.view.ProfileView
import com.dominic.githubdashboard.ui.screen.profile.recycler.RepositoryData
import com.dominic.githubdashboard.ui.screen.profile.recycler.RepositoryInfoAdapter
import de.hdodenhof.circleimageview.CircleImageView
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class UserProfileActivity : AppCompatActivity(), ProfileView, SwipeRefreshLayout.OnRefreshListener {

    val username :String = "jakewharton"
    val githubAccessToken:String = "Bearer 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"; // please add a personal access token from github

    //region views
    lateinit var rcPinned: RecyclerView
    lateinit var rcTopRepo: RecyclerView
    lateinit var rcStarredRepo:RecyclerView
    private lateinit var apiConsumer: GithubApiConsumer
    lateinit var ivProfile: CircleImageView
    lateinit var tvFullName: TextView
    lateinit var tvUsername: TextView
    lateinit var tvEmail: TextView
    lateinit var tvFollowersCount: TextView
    lateinit var tvFollowingCount: TextView
    lateinit var swipeRefreshLayout: SwipeRefreshLayout
    //endregion

    //region presenter
    private lateinit var githubProfilePresenter: GithubProfilePresenter
    //endregion

    //region adapters
    lateinit var pinnedReposAdapter: RepositoryInfoAdapter
    lateinit var topReposAdapter: RepositoryInfoAdapter
    lateinit var starredReposAdapter: RepositoryInfoAdapter
    //endregion

    //region
    private var pinnedRepos: ArrayList<RepositoryData?> = ArrayList()
    private var topRepos: ArrayList<RepositoryData?> = ArrayList()
    private var starredRepos: ArrayList<RepositoryData?> = ArrayList()
    //endregion

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide() //<< this

        setContentView(R.layout.activity_main)
        bindViews()

        configureRecyclerView()

        initValues()

        githubProfilePresenter.loadUserProfile(username)
    }

    private fun configureRecyclerView() {

        pinnedReposAdapter = RepositoryInfoAdapter(pinnedRepos,false)
        rcPinned.adapter = pinnedReposAdapter

        topReposAdapter = RepositoryInfoAdapter(topRepos,true);
        rcTopRepo.adapter = topReposAdapter

        starredReposAdapter = RepositoryInfoAdapter(starredRepos,true)
        rcStarredRepo.adapter = starredReposAdapter

        rcPinned.layoutManager = LinearLayoutManager(this)
        rcTopRepo.layoutManager = LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        rcStarredRepo.layoutManager = LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)


    }

    private fun bindViews() {
        //        swipeLayout = findViewById(R.id.swLayout)
        rcStarredRepo = findViewById(R.id.rcStarredRepo)
        rcPinned = findViewById(R.id.rcPinned)
        rcTopRepo = findViewById(R.id.rcTopRepo)
        tvUsername = findViewById(R.id.tvUsername)
        tvFullName = findViewById(R.id.tvFullName)
        tvEmail = findViewById(R.id.tvMail)
        tvFollowersCount = findViewById(R.id.tvCountFollowers)
        tvFollowingCount = findViewById(R.id.tvCountFollowing)

        ivProfile = findViewById(R.id.ivProfile)

        swipeRefreshLayout = findViewById(R.id.swLayout)
        swipeRefreshLayout.setOnRefreshListener(this)
    }

    private fun initValues() {
        apiConsumer = GithubApiConsumer(githubAccessToken,applicationContext)
        githubProfilePresenter =
            GithubProfilePresenterImpl(this, GithubProfileModelInterfaceImpl(apiConsumer))


    }


    override fun showNetworkError(errorMessage: String) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show()
    }

    override fun showLoadingScreen() {
        Toast.makeText(this, "Retrieving user information !", Toast.LENGTH_SHORT).show()
    }

    override fun cancelSwipeToRefresh() {
        swipeRefreshLayout.isRefreshing = false
    }

    override fun showOffline() {
    }

    override fun loadProfileInfo(
        fullName: String,
        login: String,
        email: String,
        followersCount: String,
        followingCount: String,
        avatarUrl: String,
    ) {

        Log.e("tag", "loadProfileInfo: ")

        tvEmail.text = email
        tvFullName.text = fullName
        tvUsername.text = login
        tvFollowingCount.text = followingCount
        tvFollowersCount.text = followersCount

        Glide.with(this)
            .load(avatarUrl)
            .centerCrop()
            .placeholder(R.drawable.ic_profile_default)
            .into(ivProfile)
    }

    override fun showPinnedRepos(repos: GetUserProfileQuery.PinnedItems) {

        pinnedRepos.clear()

        if (repos.nodes != null) {
            repos.nodes?.map { node -> node?.asRepository }?.map { asRepository ->
                RepositoryData(
                    asRepository?.name,
                    asRepository?.owner?.login,
                    asRepository?.owner?.avatarUrl as String,
                    asRepository.description,
                    asRepository.stargazerCount,
                    asRepository.primaryLanguage?.name,
                    asRepository.primaryLanguage?.color
                )
            }?.let { pinnedRepos.addAll(it) }
        }
        pinnedReposAdapter.notifyDataSetChanged()

    }

    override fun showTopRepos(repos: GetUserProfileQuery.TopRepositories) {
        topRepos.clear()

        if (repos.nodes != null) {
            repos.nodes?.map {n -> RepositoryData(n?.name,n?.owner?.login,n?.owner?.avatarUrl as String,
                n.description,n.stargazerCount,n.primaryLanguage?.name,n.primaryLanguage?.color)
            }?.let { topRepos.addAll(it) }
        }
        topReposAdapter.notifyDataSetChanged()
    }

    override fun showStarredRepos(repos: GetUserProfileQuery.StarredRepositories) {
        starredRepos.clear()

        if(repos.nodes!=null){
            repos.nodes?.map {n -> RepositoryData(n?.name,n?.owner?.login,n?.owner?.avatarUrl as String,
                n.description,n.stargazerCount,n.primaryLanguage?.name,n.primaryLanguage?.color)
            }?.let { starredRepos.addAll(it) }
        }
        starredReposAdapter.notifyDataSetChanged()

    }

    override fun onDestroy() {
        super.onDestroy()
        githubProfilePresenter.onDestroy()
    }

    override fun onRefresh() {
        githubProfilePresenter.loadUserProfile(username)
    }

}