package com.dominic.githubdashboard.ui.screen.profile.recycler

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dominic.githubdashboard.R
import de.hdodenhof.circleimageview.CircleImageView
import java.io.FileDescriptor

class RepositoryInfoAdapter(
    private val repoItems: ArrayList<RepositoryData?>,
    private val isHorizontal: Boolean
) : RecyclerView.Adapter<RepositoryInfoAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val ivOwner: CircleImageView
        val tvUsername: TextView
        val tvRepo: TextView
        val tvDescription: TextView
        val tvCount: TextView
        val tvLanguage: TextView
        val ivLangColor: ImageView

        init {
            tvUsername = view.findViewById(R.id.tvUsername)
            ivOwner = view.findViewById(R.id.ivOwner)
            tvRepo = view.findViewById(R.id.tvRepo)
            tvDescription = view.findViewById(R.id.tvDescription)
            tvCount = view.findViewById(R.id.tvCount)
            tvLanguage = view.findViewById(R.id.tvLanguage)
            ivLangColor = view.findViewById(R.id.ivLangColor)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_repository, parent, false)

        if (isHorizontal) {
            view.layoutParams.width = 600
        }

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val repoItem = repoItems[position];



        Glide.with(holder.itemView)
            .load(repoItem?.ownerAvatar as String)
            .centerCrop()
            .placeholder(R.drawable.ic_profile_default)
            .into(holder.ivOwner)

        holder.tvUsername.text = repoItem.ownerLogin
        holder.tvLanguage.text = repoItem.primaryLanguage
        holder.tvCount.text = repoItem.stargazerCount.toString()
        holder.tvUsername.text = repoItem.ownerLogin
        holder.tvRepo.text = repoItem.name
        holder.tvDescription.text = repoItem.descriptor

        DrawableCompat.setTint(
            DrawableCompat.wrap(holder.ivLangColor.drawable),
            Color.parseColor(repoItem.primaryLanguageColor)
        );

    }

    override fun getItemCount(): Int {
        return repoItems.size
    }
}

data class RepositoryData(
    val name: String?,
    val ownerLogin: String?,
    val ownerAvatar: String?,
    val descriptor: String?,
    val stargazerCount: Int?,
    val primaryLanguage: String?,
    val primaryLanguageColor: String?
)