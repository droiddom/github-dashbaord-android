package com.dominic.githubdashboard.mvp.view

interface ProfileView {
    fun showNetworkError(errorMessage:String)
    fun showLoadingScreen()
    fun cancelSwipeToRefresh()
    fun showOffline()
    fun loadProfileInfo(fullName:String,
                        login:String,
                        email:String,
                        followersCount:String,
                        followingCount:String,
                        avatarUrl:String)
    fun showPinnedRepos(repos: GetUserProfileQuery.PinnedItems)
    fun showTopRepos(repos:GetUserProfileQuery.TopRepositories)
    fun showStarredRepos(repos:GetUserProfileQuery.StarredRepositories)

}