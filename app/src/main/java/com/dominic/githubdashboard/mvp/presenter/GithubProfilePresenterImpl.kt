package com.dominic.githubdashboard.mvp.presenter

import com.dominic.github_api_consumer.Profile
import com.dominic.githubdashboard.mvp.model.GithubProfileModelInterface
import com.dominic.githubdashboard.mvp.view.ProfileView
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers

class GithubProfilePresenterImpl(
    private var profileView: ProfileView?,
    private val githubProfileModelInterface: GithubProfileModelInterface
) : GithubProfilePresenter {


    private val compositeDisposable: CompositeDisposable = CompositeDisposable()


    override fun loadUserProfile(username: String) {
        profileView?.showLoadingScreen()

        compositeDisposable.add(
            githubProfileModelInterface
                .retrieveGithubProfile(username)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    populateProfile(it)

                }, {
                    profileView?.showNetworkError(it.message as String+"\nPossibly Caused by invalid token")
                    profileView?.cancelSwipeToRefresh()
                    githubProfileModelInterface.clearCacheOnNetworkIssue()
                },{
                    profileView?.cancelSwipeToRefresh()
                })
        )
    }

    private fun populateProfile(profile: Profile) {
        profileView?.loadProfileInfo(
            profile.name,
            profile.login,
            profile.email,
            profile.followersCount.toString(),
            profile.followingCount.toString(),
            profile.avatarUrl
        )

        profileView?.showPinnedRepos(profile.pinnedRepo)
        profileView?.showTopRepos(profile.topRepositories)
        profileView?.showStarredRepos(profile.starredRepositories)
    }

    override fun onDestroy() {
        if (!compositeDisposable.isDisposed) compositeDisposable.dispose()
        profileView = null
    }
}