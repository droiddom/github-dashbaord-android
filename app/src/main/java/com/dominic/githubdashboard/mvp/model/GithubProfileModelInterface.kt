package com.dominic.githubdashboard.mvp.model

import com.dominic.github_api_consumer.Profile
import io.reactivex.rxjava3.annotations.NonNull
import io.reactivex.rxjava3.core.Observable

interface GithubProfileModelInterface {
    fun retrieveGithubProfile(username:String): @NonNull Observable<Profile>

    fun clearCacheOnNetworkIssue()
}