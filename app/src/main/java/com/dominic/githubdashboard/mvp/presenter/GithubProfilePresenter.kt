package com.dominic.githubdashboard.mvp.presenter

interface GithubProfilePresenter {
    fun loadUserProfile(username: String)
    fun onDestroy()
}