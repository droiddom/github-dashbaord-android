package com.dominic.githubdashboard.mvp.model

import com.dominic.github_api_consumer.GithubApiConsumer
import com.dominic.github_api_consumer.Profile
import io.reactivex.rxjava3.annotations.NonNull
import io.reactivex.rxjava3.core.Observable

class GithubProfileModelInterfaceImpl(val apiConsumer: GithubApiConsumer) :GithubProfileModelInterface {


    override fun retrieveGithubProfile(username: String): @NonNull Observable<Profile> {
        return apiConsumer.retrieveUserProfileObs(username);
    }

    override fun clearCacheOnNetworkIssue() {
        apiConsumer.clearCache()
    }


}