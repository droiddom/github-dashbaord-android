package com.dominic.github_api_consumer

import GetUserProfileQuery
import android.util.Log
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.coroutines.await
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

import java.lang.Exception

@RunWith(AndroidJUnit4::class)
class GithubConsumerTest{

    private val mainThreadSurrogate = newSingleThreadContext("UI thread")
    val TAG = "ApolloTest"
    private lateinit var apiConsumer:GithubApiConsumer

    @Before
    fun setUp() {
        apiConsumer = GithubApiConsumer("Bearer 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",ApplicationProvider.getApplicationContext())

        Dispatchers.setMain(mainThreadSurrogate)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
        mainThreadSurrogate.close()
    }

    @Test
    fun apolloClientTest(): Unit = runBlocking {
        val token = "Bearer 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";

        try {
            val response = apiConsumer.apolloClient
                .query(GetUserProfileQuery("jakewharton")).await();

            assert(!response.hasErrors())

        }catch (e:Exception){

            Log.e(TAG, "apolloClientTest: ", e)
        }



    }

    @Test
    fun test(){
        assert(1==1)
    }


}