package com.dominic.github_api_consumer

import GetUserProfileQuery
import android.util.Log
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.coroutines.await
import com.apollographql.apollo.rx3.rxQuery
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.lang.Exception

class ApolloTest {
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    val TAG = "ApolloTest"


    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
        mainThreadSurrogate.close()
    }


    @Test
    fun testApolloImplementation() = runBlocking {

        val token = "Bearer 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";

        val apolloClient = ApolloClient.builder()
            .serverUrl("https://api.github.com/graphql")
            .okHttpClient(
                OkHttpClient.Builder()
                    .addInterceptor(AuthorizationInterceptor(token))
                    .build()
            )
            .build()

        try {
            val response = apolloClient.query(GetUserProfileQuery("JakeWharton")).await()



            assert(!response.hasErrors())

        } catch (e: Exception) {
            Log.e(TAG, "testApolloImplementation: ", e)
        }


    }

    @Test
    fun testApolloRx() = runBlocking {
        val token = "Bearer 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";

        val apolloClient = ApolloClient.builder()
            .serverUrl("https://api.github.com/graphql")
            .okHttpClient(
                OkHttpClient.Builder()
                    .addInterceptor(AuthorizationInterceptor(token))
                    .build()
            )
            .build()

        try {

            apolloClient.rxQuery(GetUserProfileQuery("JakeWharton"))
//                .subscribeOn(Schedulers.trampoline())
                .observeOn(Schedulers.trampoline())
                .subscribe(
                    {
                        val data = it
                        Log.d(TAG, "testApolloRx: "+data.data?.user?.login)
                        assert(data!=null)
                    },
                    {

                    }
                )

        } catch (e: Exception) {
            Log.e(TAG, "testApolloImplementation: ", e)
        }
    }
}

private class AuthorizationInterceptor(val token: String) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder()
            .addHeader("Authorization", token)
            .build()

        return chain.proceed(request)
    }
}
