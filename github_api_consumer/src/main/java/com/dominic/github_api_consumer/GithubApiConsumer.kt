package com.dominic.github_api_consumer

import GetUserProfileQuery
import android.content.Context
import androidx.core.content.ContextCompat
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.api.cache.http.HttpCachePolicy
import com.apollographql.apollo.cache.http.ApolloHttpCache
import com.apollographql.apollo.cache.http.DiskLruHttpCacheStore
import com.apollographql.apollo.coroutines.await
import com.apollographql.apollo.rx3.rxQuery
import io.reactivex.rxjava3.annotations.NonNull
import io.reactivex.rxjava3.core.Observable
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import java.io.File
import java.util.concurrent.TimeUnit

interface GithubApiConsumerInterface {

    suspend fun retrieveUserProfile(user: String): GetUserProfileQuery.User?

    fun retrieveUserProfileObs(user: String): @NonNull Observable<Profile>

    fun clearCache();
}


class GithubApiConsumer(private val token: String, private val context: Context) :
    GithubApiConsumerInterface {

    val apolloClient: ApolloClient = ApolloClient.builder()

        .serverUrl("https://api.github.com/graphql")
        .okHttpClient(
            OkHttpClient.Builder()
                .addInterceptor(AuthorizationInterceptor(token))
                .build()
        )
        .httpCache(ApolloHttpCache(getCache()))
        .defaultHttpCachePolicy(HttpCachePolicy.CACHE_FIRST.expireAfter(1, TimeUnit.DAYS))
        .build()


    override suspend fun retrieveUserProfile(user: String): GetUserProfileQuery.User? {


        return try {
            val response = apolloClient.query(GetUserProfileQuery(user)).await()

            response.data?.user

        } catch (e: Exception) {

            null;
        }

    }

    fun getCache(): DiskLruHttpCacheStore {
        val file = File(context.cacheDir, "apolloCache")

        val size: Long = 1024 * 1024
        val cacheStore = DiskLruHttpCacheStore(file, size)

        return cacheStore
    }

    override fun retrieveUserProfileObs(user: String): @NonNull Observable<Profile> {

        return apolloClient
            .rxQuery(GetUserProfileQuery(user))
//            .filter { res -> res.data != null }
            .map { res -> res.data as GetUserProfileQuery.Data }
            .map { data -> data.user as GetUserProfileQuery.User }
            .map { user1 ->
                Profile(
                    user1.email as String,
                    user1.name as String,
                    user1.avatarUrl as String,
                    user1.login,
                    user1.followers.totalCount,
                    user1.following.totalCount,
                    user1.pinnedItems,
                    user1.topRepositories,
                    user1.starredRepositories,
                )
            }
//            .doOnError({error -> "hello"})
    }

    override fun clearCache() {
        apolloClient.clearHttpCache()
    }
}

private class AuthorizationInterceptor(val token: String) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder()
            .addHeader("Authorization", token)
            .build()

        return chain.proceed(request)
    }
}

data class Profile(
    val email: String,
    val name: String,
    val avatarUrl: String,
    val login: String,
    val followersCount: Int,
    val followingCount: Int,
    val pinnedRepo: GetUserProfileQuery.PinnedItems,
    val topRepositories: GetUserProfileQuery.TopRepositories,
    val starredRepositories: GetUserProfileQuery.StarredRepositories,
)
